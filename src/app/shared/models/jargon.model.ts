export class Jargon {
	id: number;
	title: string;
	explanation: string;
	category: string;
}
