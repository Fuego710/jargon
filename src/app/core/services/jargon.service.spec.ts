import { TestBed } from '@angular/core/testing';

import { JargonService } from './jargon.service';

describe('JargonService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));

	it('should be created', () => {
		const service: JargonService = TestBed.get(JargonService);
		expect(service).toBeTruthy();
	});
});
