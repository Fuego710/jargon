import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Jargon } from '../../shared/models/jargon.model';

@Injectable({
	providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

	createDb() {
		const jargons = [
			{
				id: 1,
				title: 'Jargon Test',
				explanation: 'This is what the jargon means',
				category: 'testCat'
			},
			{
				id: 2,
				title: 'Another Jargon Test',
				explanation: 'This is some more jargon',
				category: 'anotherCat'
			},
		];

		return { jargons };
	}

	genId(jargons: Jargon[]): number {
		return jargons.length > 0 ? Math.max(...jargons.map(jargon => jargon.id)) + 1 : 11;
	}

}
