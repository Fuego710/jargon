import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddJargonComponent } from './add-jargon.component';

describe('AddJargonComponent', () => {
	let component: AddJargonComponent;
	let fixture: ComponentFixture<AddJargonComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AddJargonComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddJargonComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
