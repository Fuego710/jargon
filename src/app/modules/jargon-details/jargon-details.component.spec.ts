import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JargonDetailsComponent } from './jargon-details.component';

describe('JargonDetailsComponent', () => {
	let component: JargonDetailsComponent;
	let fixture: ComponentFixture<JargonDetailsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [JargonDetailsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(JargonDetailsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
