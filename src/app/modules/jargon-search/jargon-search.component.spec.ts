import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JargonSearchComponent } from './jargon-search.component';

describe('JargonSearchComponent', () => {
	let component: JargonSearchComponent;
	let fixture: ComponentFixture<JargonSearchComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [JargonSearchComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(JargonSearchComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
