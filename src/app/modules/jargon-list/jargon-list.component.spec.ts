import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JargonListComponent } from './jargon-list.component';

describe('JargonListComponent', () => {
	let component: JargonListComponent;
	let fixture: ComponentFixture<JargonListComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [JargonListComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(JargonListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
