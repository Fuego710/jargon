import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JargonListComponent } from './modules/jargon-list/jargon-list.component';
import { JargonSearchComponent } from './modules/jargon-search/jargon-search.component';
import { JargonDetailsComponent } from './modules/jargon-details/jargon-details.component';
import { AddJargonComponent } from './modules/add-jargon/add-jargon.component';

@NgModule({
	declarations: [
		AppComponent,
		JargonListComponent,
		JargonSearchComponent,
		JargonDetailsComponent,
		AddJargonComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
